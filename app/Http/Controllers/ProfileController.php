<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class profileController extends Controller
{
    public function getIndex($id)
    {
        $user = User::findOrFail($id);
        return view('citidex.profile', ['user' => $user]);
    }
    public function getEdit($id)
    {
        $user = User::findOrFail($id);
        return view('profile.edit', ['user' => $user]);
    }
    public function postEdit($id)
    {
        $user = User::findOrFail($id);
        return view('profile.edit', ['user' => $user]);
    }
}
