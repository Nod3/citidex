<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Ship;
use App\Location;
use App\SolarSystem;
use App\Moon;
use App\Station;
use App\Planet;
use App\AsteroidCluster;

class AdminController extends Controller
{
    //Ships
    public function getIndex()
    {
        return view('admin.ship');
    }

    public function postStoreShip(Request $request)
    {

        $ship = new Ship($request->only([
            'name',
            'manufacturer',
            'type',
            'cargo',
            'description',
        ]));

        if ($request->hasFile('picture')) {
            $getimgname = $request->file('picture')->store('public/images');
            $ship->picture = '/storage/images/' . basename($getimgname);
        }
        $ship->save();
        return redirect('/admin/ship')->with('success', 'saved!');
    }

    // Locations
    public function getIndexLocation()
    {
        $locations = Location::all();
        return view('admin.location', ['locations' => $locations]);
    }

    public function postStoreLocation(Request $request)
    {
        $location = new Location($request->only([
            'name',
            'type',
        ]));
        if ($request->has('parent')) {
            $location->parent_id = $request->get('parent');
        }
        $location->save();

        if ($request->has('parent')) {
            $location->parent()->attach($request->get('parent'),['location_id' => $location->location_id]);
        }
        return redirect('/admin/location')->with('success', 'saved!');
    }
}
