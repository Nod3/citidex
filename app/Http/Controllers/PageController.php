<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\Ship;

class PageController extends Controller
{
    public function listShips()
    {
        $ships = Ship::all();
        return view('page.ships', [ 'ships' => $ships ]);
    }
    public function listLocations(Request $request)
    {

        $locations = Location::all();
        $filter = Location::where('type', $request->get('type'));
        return view('page.locations.index', [ 'locations' => $locations, 'filter' => $filter ]);
    }
}
