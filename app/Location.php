<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LocationChildren;

class Location extends Model
{
    protected $fillable = [
        'name',
        'type',
        'parent_id'
    ];
    protected $primaryKey = 'location_id';
    protected $table = 'locations';
    public $timestamps = false;

    public function children()
    {
        return $this->hasMany(
            Location::class,
            'parent_id',
            'location_id');
    }
    public function parent()
    {
        return $this->belongsToMany(
            Location::class,
            'locations_children',
            'location_id',
            'parent_id');
    }

}
