<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationChildren extends Model
{
    protected $fillable = [
        'location_id',
        'parent_id',
    ];
    protected $table = 'locations_children';
    public $timestamps = false;
}
