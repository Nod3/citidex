<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    protected $fillable = [
        'name',
    ];
    protected $primaryKey = 'station_id';
    protected $table = 'stations';
    public $timestamps = false;
}
