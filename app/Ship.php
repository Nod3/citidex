<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    protected $fillable = [
        'ship_id',
        'manufacturer',
        'name',
        'type',
        'description',
        'picture',
        'cargo',
    ];
    protected $primaryKey = 'ship_id';
    protected $table = 'ships';
    public $timestamps = false;
}
