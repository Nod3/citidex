<?php
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('citidex.index');
    });
});

Auth::routes();

Route::get('/', 'HomeController@getIndex')->name('home');

/*Pages*/
Route::group(['middleware' => 'auth'], function () {
    Route::prefix('page')->group(function () {
        Route::get('/ships', 'PageController@listShips')->name('page.ships');
        Route::get('/locations/{type?}', 'PageController@listLocations')->name('page.locations');
    });
});


/*Profile*/
Route::group(['middleware' => 'auth'], function () {
    Route::prefix('profile')->group(function () {
        Route::get('/{id}', 'ProfileController@getIndex')->name('citidex.profile');
        Route::get('/edit/{id}', 'ProfileController@getEdit')->name('profile.edit');
        Route::post('/edit/{id}', 'ProfileController@postEdit')->name('profile.editPost');
    });
});

/*Admin*/
Route::group(['middleware' => 'auth'], function () {
    Route::prefix('admin')->group(function () {
        Route::prefix('ship')->group(function () {
            Route::get('/', 'AdminController@getIndex')->name('admin.ship');
            Route::get('/edit/{id}', 'AdminController@getEditShip')->name('admin.editship');
            Route::post('/', 'AdminController@postStoreShip')->name('admin.storeship');
        });

        Route::prefix('location')->group(function () {
            Route::get('/', 'AdminController@getIndexLocation');
            Route::post('/', 'AdminController@postStoreLocation')->name('admin.storelocation');
        });
    });
});

/*Admin*/
Route::group(['middleware' => 'auth'], function () {
    Route::prefix('admin')->group(function () {
        Route::prefix('ship')->group(function () {
            Route::get('/', 'AdminController@getIndex')->name('admin.ship');
            Route::get('/edit/{id}', 'AdminController@getEditShip')->name('admin.editship');
            Route::post('/', 'AdminController@postStoreShip')->name('admin.storeship');
        });

        Route::prefix('location')->group(function () {
            Route::get('/', 'AdminController@getIndexLocation');
            Route::post('/', 'AdminController@postStoreLocation')->name('admin.storelocation');
        });
    });
});