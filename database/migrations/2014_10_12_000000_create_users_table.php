<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->unique();
            $table->string('rsi')->nullable();
            $table->string('password');
            $table->string('picpath')->default("/img/avatar.png");
            $table->longText('bio')->nullable();
            $table->enum('timezone', ['Eastern', 'Central', 'Mountain', 'Pacific'])->nullable();
            $table->string('steam')->nullable();
            $table->string('twitch')->nullable();
            $table->string('discord')->nullable();
            $table->string('origin')->nullable();
            $table->string('battlenet')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
