<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipsTable extends Migration
{
    public $timestamps = false;

    public function up()
    {
        Schema::create('ships', function (Blueprint $table) {
            $table->bigIncrements('ship_id');
            $table->string('picture')->nullable();
            $table->string('description')->nullable();
            $table->string('manufacturer')->default('NEED DATA');
            $table->string('name')->unique();
            $table->string('type')->default('NEED DATA');
            $table->integer('cargo')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ships');
    }
}
