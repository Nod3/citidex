<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LocationsChildren extends Migration
{
    public function up()
    {
        Schema::create('locations_children', function (Blueprint $table) {
            $table->integer('location_id');
            $table->integer('parent_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('planet_children');
    }
}
