@extends('layouts.citidex')

@section('citidex')
    <div class="slim-mainpanel">
        <div class="container">

            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="/admin">Page</a></li>
                    <li class="breadcrumb-item active" aria-current="page">All Ships</li>
                </ol>
                <h6 class="slim-pagetitle">Ships Page</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper mg-t-20">
                <div class="card-columns">
                    @foreach($ships as $ship)
                        <div class="col-md">
                            <div class="card bd-danger">
                                <img class="img-fluid" src="{{$ship->picture}}" alt="Image">
                                <div class="card-img-overlay p-2 pd-30 bg-black-4 d-flex flex-column justify-content-center">
                                    <p class="tx-white tx-medium mg-b-15">{{$ship->manufacturer}}&nbsp<b class="tx-success">{{$ship->name}}</b></p>
                                    <p class="tx-white-7 tx-13">{!! Str::limit($ship->description, 120, ' ...') !!}</p>
                                    <p class="tx-white-7">CARGO: <b class="tx-success">{{$ship->cargo}}</b></p>
                                    <p class="mg-b-0"><a href="" class="tx-info">Goto Ship</a></p>
                                </div><!-- card-img-overlay -->
                            </div><!-- card -->
                        </div><!-- col -->
                    @endforeach
                </div><!-- row -->
            </div><!-- section-wrapper -->
        </div>
    </div>
@endsection