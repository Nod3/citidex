@extends('layouts.citidex')
@section('header')
    <link href="/citidex/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="/citidex/lib/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="/citidex/lib/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <link href="/citidex/lib/select2/css/select2.min.css" rel="stylesheet">

@endsection

@section('citidex')
    <div class="slim-mainpanel">
        <div class="container">
            <div class="manager-header">
                <div class="slim-pageheader">
                    <ol class="breadcrumb slim-breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Locations</li>
                    </ol>
                    <h6 class="slim-pagetitle">Contact Manager</h6>
                </div><!-- slim-pageheader -->
                <a id="contactNavicon" href="" class="contact-navicon"><i class="icon ion-navicon-round"></i></a>
            </div><!-- manager-header -->

            <div class="manager-wrapper">
                <div class="manager-right">
                    <div class="row row-sm">

                        @foreach($locations as $location)
                            <div class="col-sm-6 col-lg-4">
                                <div class="card-contact">
                                    <div class="tx-center">
                                        @switch($location->type)
                                            @case('SolarSystem')
                                            <a href=""><img src="/svg/SolarSystem.svg" class="card-img"></a>
                                            @break
                                            @case('Planet')
                                            <a href=""><img src="/svg/Planet.svg" class="card-img"></a>
                                            @break
                                            @case('Moon')
                                            <a href=""><img src="/svg/Moonsvg.svg" class="card-img"></a>
                                            @break
                                            @case('Station')
                                            <a href=""><img src="/svg/Satt.svg" class="card-img"></a>
                                            @break
                                            @case('AsteroidCluster')
                                            <a href=""><img src="/svg/Asteroidsvg.svg" class="card-img"></a>
                                            @break
                                        @endswitch
                                        <h5 class="mg-t-10 mg-b-5"><a href=""
                                                                      class="contact-name">{{$location->name}}</a></h5>
                                        <p class="align-items-center">
                                            <a href=""><i class="ion ion-android-list"></i>&nbsp&nbsp{{$location->type}}</a>
                                        </p>
                                    </div><!-- tx-center -->

                                    <p class="contact-item">
                                        <span>Orbits:</span>
                                        <span>{{$location->parent()->value('name')}}</span>
                                    </p><!-- contact-item -->
                                    <p class="contact-item">
                                        <span>Satellite Bodies:</span>
                                        <a>{{$location->children()->count()}}</a>
                                    </p><!-- contact-item -->
                                    <p class="contact-item">
                                        <span>URL:</span>
                                        <a href="">http://thmpxls.me</a>
                                    </p><!-- contact-item -->
                                </div><!-- card -->
                            </div><!-- col -->
                        @endforeach

                    </div><!-- row -->

                    <a href="" class="btn btn-outline-secondary btn-block mg-t-20">Load more contacts</a>

                </div><!-- manager-right -->
                <div class="manager-left">
                    <a href="" class="btn btn-contact-new">Add New</a>
                    <nav class="nav">
                        <a href="" class="nav-link active">
                            <span>All Contacts</span>
                            <span>120</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>Family</span>
                            <span>16</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>Friends</span>
                            <span>68</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>Co-workers</span>
                            <span>38</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>Favorites</span>
                            <span>9</span>
                        </a>
                    </nav>

                    <label class="section-label-sm mg-t-25">Location</label>
                    <nav class="nav">
                        <a href="" class="nav-link">
                            <span>San Francisco</span>
                            <span>10</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>Los Angeles</span>
                            <span>16</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>New York</span>
                            <span>15</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>Las Vegas</span>
                            <span>4</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>Sacramento</span>
                            <span>4</span>
                        </a>
                    </nav>

                    <label class="section-label-sm mg-t-25">Job Position</label>
                    <nav class="nav">
                        <a href="" class="nav-link">
                            <span>Software Engineer</span>
                            <span>4</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>UI Designer</span>
                            <span>6</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>Sales Representative</span>
                            <span>5</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>Mechanical Engineer</span>
                            <span>4</span>
                        </a>
                        <a href="" class="nav-link">
                            <span>Nurse</span>
                            <span>4</span>
                        </a>
                    </nav>
                </div><!-- manager-left -->
            </div><!-- manager-wrapper -->

        </div><!-- container -->
    </div><!-- slim-mainpanel -->
@endsection

@section('scripts')
@endsection