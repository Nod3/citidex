@extends('layouts.citidex')
@section('header')
    <link href="/citidex/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="/citidex/lib/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="/citidex/lib/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <link href="/citidex/lib/select2/css/select2.min.css" rel="stylesheet">

@endsection

@section('citidex')
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create Ship</li>
                </ol>
                <h6 class="slim-pagetitle">Creating New Ship</h6>
            </div><!-- slim-pageheader -->

            <form method="post" action="{{ route('admin.storeship') }}" enctype="multipart/form-data">
                @csrf
                <div class="section-wrapper mg-t-20">
                    <div class="d-flex">
                        <h2 class="text-primary col-6">New Ship Basic Details</h2>
                        <select id="select10" name="type" class="form-control pull-right" data-placeholder="Ship Type">
                            <option label="Choose one"></option>
                            <option value="Combat">Combat</option>
                            <option value="Combat Transport">Combat Transport</option>
                            <option value="Cargo Transport">Cargo Transport</option>
                            <option value="Mining">Mining</option>
                            <option value="Racing">Racing</option>
                        </select>
                    </div>
                    <hr>
                    <p class="mg-b-20 mg-sm-b-40 text-dark">Basic details about the ship you're creating into the
                        database;
                        You've been
                        permitted as an admin, so please don't FUCK up the data.
                        Or else I'll have Magic Mason XXL give you a lapdance.</p>
                    <div class="form-layout form-layout-7">
                        <div class="row no-gutters">
                            <div class="col-5 col-sm-4">
                                Ship Manufacturer:
                            </div><!-- col-4 -->
                            <div class="col-7 col-sm-8">
                                <input class="form-control" type="text" name="manufacturer"
                                       placeholder="Enter ship manufacturer...">
                            </div><!-- col-8 -->
                        </div><!-- row -->

                        <div class="row no-gutters">
                            <div class="col-5 col-sm-4">
                                Ship Name:
                            </div><!-- col-4 -->
                            <div class="col-7 col-sm-8">
                                <input class="form-control" type="text" name="name" placeholder="Enter ship name...">
                            </div><!-- col-8 -->
                        </div><!-- row -->

                        <div class="row no-gutters">
                            <div class="col-5 col-sm-4">
                                Description:
                            </div><!-- col-4 -->
                            <div class="col-7 col-sm-8">
                        <textarea class="form-control" name="description"
                                  placeholder="Enter a brief description..."></textarea>
                            </div><!-- col-8 -->
                        </div><!-- row -->
                        <br>
                        <hr>
                        <label class="section-title">Max Cargo Limit:</label>
                        <input type="text" data-extra-classes="irs-modern irs-orange" class="js-range-slider"
                               name="cargo"
                               value=""
                               data-min="0"
                               data-max="1000"
                               data-to="500"
                               data-step="10"
                               data-grid="true"/>
                    </div><!-- form-layout -->
                    <hr>
                    <div class="d-flex">
                        <h2 class="col-5">Ship Image:</h2>
                        <div class="col-md-6 offset-1">
                            <label class="custom-file-label" for="customFile">Choose Image for Ship</label>
                            <input type="file" class="custom-file-input" id="customFile" name="picture">
                        </div>
                        <hr>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-outline-pink btn-block">Submit Ship</button>
            </form>
        </div><!-- section-wrapper -->
    </div>
    </div>
@endsection

@section('scripts')

    <script src="/citidex/lib/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
    <script src="/citidex/lib/select2/js/select2.min.js"></script>
    <script>
        $(".js-range-slider").ionRangeSlider({
            onFinish: function (data) {
                // fired on pointer release
            }
        });
    </script>
    <script>
        $(function () {
            'use strict';

            $('#select10').select2({
                containerCssClass: 'select2-full-color select2-indigo',
                dropdownCssClass: 'select2-drop-color select2-drop-indigo',
                minimumResultsForSearch: Infinity // disabling search
            });
        });
    </script>
    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function () {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>
@endsection