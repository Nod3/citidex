@extends('layouts.citidex')
@section('header')
    <link href="/citidex/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="/citidex/lib/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="/citidex/lib/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <link href="/citidex/lib/select2/css/select2.min.css" rel="stylesheet">

@endsection

@section('citidex')
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create Solar System</li>
                </ol>
                <h6 class="slim-pagetitle">Creating New Solar System</h6>
            </div><!-- slim-pageheader -->

            <form method="post" action="{{ route('solar.store') }}">
                @csrf
                <div class="section-wrapper mg-t-20">
                    <div class="d-flex">
                        <h2 class="text-warning col-6">New Solar System Basic Details</h2>
                    </div>
                    <hr>
                    <p class="mg-b-20 mg-sm-b-40 text-white-50">Basic details about the Solar System you're creating into the
                        database;
                        You've been
                        permitted as an admin, so please don't FUCK up the data.
                        Or else I'll have Magic Mason XXL give you a lapdance.</p>
                    <div class="form-layout form-layout-7">

                        <div class="row no-gutters">
                            <div class="col-5 col-sm-4">
                                Solar System Name:
                            </div><!-- col-4 -->
                            <div class="col-7 col-sm-8">
                                <input class="form-control" type="text" name="name" placeholder="Enter Solar System name...">
                            </div><!-- col-8 -->
                        </div><!-- row -->

                        <div class="row no-gutters">
                            <div class="col-5 col-sm-4">
                                Description:
                            </div><!-- col-4 -->
                            <div class="col-7 col-sm-8">
                        <textarea class="form-control" name="description"
                                  placeholder="Enter a brief description..."></textarea>
                            </div><!-- col-8 -->
                        </div><!-- row -->
                    </div>
                    <br><hr>
                    <button type="submit" class="btn btn-outline-warning btn-block">Submit Solar System</button>
            </form>
        </div><!-- section-wrapper -->

    </div>
    </div>
@endsection

@section('scripts')
@endsection