@extends('layouts.citidex')
@section('header')
    <link href="/citidex/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="/citidex/lib/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="/citidex/lib/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <link href="/citidex/lib/select2/css/select2.min.css" rel="stylesheet">

@endsection

@section('citidex')
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Create Location</li>
                </ol>
                <h6 class="slim-pagetitle">Creating New Location</h6>
            </div><!-- slim-pageheader -->

            <form method="post" action="{{ route('admin.storelocation') }}">
                @csrf
                <div class="section-wrapper mg-t-20">
                    <div class="d-flex">
                        <h2 class="text-primary col-6">New Location Basic Details</h2>
                        <select id="select10" name="type" class="form-control pull-right"
                                data-placeholder="Location Type">
                            <option label="Location Type"></option>
                            <option value="SolarSystem">Solar System</option>
                            <option value="Planet">Planet</option>
                            <option value="Moon">Moon</option>
                            <option value="Station">Station</option>
                            <option value="AsteroidCluster">Asteroid Cluster</option>
                        </select>
                    </div>
                    <hr>
                    <p class="mg-b-20 mg-sm-b-40 text-white-50">Basic details about the Location you're creating into
                        the
                        database;
                        You've been
                        permitted as an admin, so please don't FUCK up the data.
                        Or else I'll have Magic Mason XXL give you a lapdance.</p>
                    <div class="form-layout form-layout-7">
                        <div class="row no-gutters">
                            <div class="col-5 col-sm-4">
                                Location Name:
                            </div><!-- col-4 -->
                            <div class="col-7 col-sm-8">
                                <select id="select10" name="parent" class="form-control pull-right"
                                        data-placeholder="Location Type">
                                    <option label="Parent Location"></option>
                                    @foreach($locations as $parent)
                                        <option value="{{$parent->location_id}}">{{$parent->name}}</option>
                                    @endforeach
                                </select>
                            </div><!-- col-8 -->
                        </div><!-- row -->
                        <div class="row no-gutters">
                            <div class="col-5 col-sm-4">
                                Location Name:
                            </div><!-- col-4 -->
                            <div class="col-7 col-sm-8">
                                <input class="form-control" type="text" name="name"
                                       placeholder="Enter location name...">
                            </div><!-- col-8 -->
                        </div><!-- row -->
                    </div>
                    <br>
                    <hr>
                    <button type="submit" class="btn btn-outline-pink btn-block">Submit location</button>
            </form>
        </div><!-- section-wrapper -->

    </div>
    </div>
@endsection

@section('scripts')
@endsection