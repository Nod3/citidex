@extends('layouts.citidex')

@section('citidex')

    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Pages</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Profile Page</li>
                </ol>
                <h6 class="slim-pagetitle">Editing Profile</h6>
            </div><!-- slim-pageheader -->

            <div class="row row-sm">
                <div class="col-lg-8">
                    <div class="card card-profile">
                        <div class="card-body"><p class="text-danger">* MUST SUBMIT BEFORE VISIBLE</p>
                            <form method="post" action="{{ route('profile.editPost', $user->id) }}" enctype="multipart/form-data">
                            <div class="media">
                                    @csrf
                                <div class="image8 image-upload">
                                    <label for="file-input">
                                        <img src="/{{$user->picpath}}" alt="">
                                    </label>
                                    <input name="picpath" id="picpath" type="file"/>
                                </div><br>
                                <div class="media-body">
                                    <h3 class="card-profile-name">{{$user->username}}</h3>
                                    <p class="card-profile-position"><b style="color:yellow">Captain</b> of a <u>RSI Freelancer Max</u></p><br>
                                    <select class="form-control form-control-sm" name="timezone" id="timezone">
                                        <option value="Eastern">Eastern Standard Time (EST)</option>
                                        <option value="Central">Central Standard Time (CST)</option>
                                        <option value="Mountain">Mountain Standard Time (MST)</option>
                                        <option value="Pacific">Pacific Standard Time (PST)</option>
                                    </select>
                                    <br>
                                    <textarea class="form-control" name="bio" id="bio" rows="3"></textarea>
                                </div><!-- media-body -->

                            </div><!-- media -->
                            </form>
                        </div><!-- card-body -->
                        <div class="card-footer">
                            <button class="btn btn-success btn-block" type="submit">Submit</button>
                        </div><!-- card-footer -->
                    </div><!-- card -->

                    <ul class="nav nav-activity-profile mg-t-20">
                        <li class="nav-item"><a href="" class="nav-link"><i class="icon ion-ios-redo tx-purple"></i> Add New Ship</a></li>
                        <li class="nav-item"><a href="" class="nav-link"><i class="icon ion-image tx-primary"></i> Submit Trade Data</a></li>
                        <li class="nav-item"><a href="" class="nav-link"><i class="icon ion-document-text tx-success"></i> Submit Mining Data</a></li>
                    </ul><!-- nav -->

                    <div class="card card-experience mg-t-20">
                        <div class="card-body">
                            <div class="slim-card-title">Division & Team</div>
                            <hr>
                            <div class="media">
                                <div class="experience-logo">
                                    <i class="icon ion-disc"></i>
                                </div><!-- experience-logo -->
                                <div class="media-body">
                                    <h5 class="position-name">Security Divsion</h5>
                                    <p class="position-company">Diamonds Squad</p>
                                    <p class="position-year">Squad Leader: Since Nov 21, 2018</p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                        </div><!-- card-body -->
                        <div class="card-footer">
                            <a href="">Show more<span class="d-none d-sm-inline"> experiences (4)</span> <i class="fa fa-angle-down"></i></a>
                            <a href="">Add new</a>
                        </div><!-- card-footer -->
                    </div><!-- card -->

                    <div class="card card-recommendation mg-t-20">
                        <div class="card-body pd-25">
                            <div class="slim-card-title">Awards</div>
                            <div class="media align-items-center mg-y-25">
                                <img src="http://via.placeholder.com/500x500" class="wd-40 rounded-circle" alt="">
                                <div class="media-body mg-l-15">
                                    <h6 class="tx-14 mg-b-2"><a href="">Black Market Investor</a></h6>
                                    <p class="mg-b-0">Economy</p>
                                </div><!-- media-body -->
                                <span class="tx-12">Nov 20, 2017</span>
                            </div><!-- media -->
                            <p>Spend atleast 1m on Illegal Vices</p>

                            <hr size="30">

                            <div class="media align-items-center mg-y-25">
                                <img src="http://via.placeholder.com/500x500" class="wd-40 rounded-circle" alt="">
                                <div class="media-body mg-l-15">
                                    <h6 class="tx-14 mg-b-2"><a href="">Black Market Investor</a></h6>
                                    <p class="mg-b-0">Economy</p>
                                </div><!-- media-body -->
                                <span class="tx-12">Nov 20, 2017</span>
                            </div><!-- media -->
                            <p>Spend atleast 1m on Illegal Vices</p>
                        </div><!-- card-body -->

                        <div class="card-footer pd-y-12 pd-x-25 bd-t bd-gray-300">
                            <a href="">More recommendations (2) <i class="fa fa-angle-down"></i></a>
                        </div><!-- card-footer -->
                    </div>
                </div><!-- col-8 -->

                <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                    <div class="card card-connection">
                        <div class="row row-xs">
                            <div class="col-4 tx-primary">0</div>
                            <div class="col-8">Trade Data Submissions</div>
                        </div><!-- row -->
                        <hr>
                        <div class="row row-xs">
                            <div class="col-4 text-success">0</div>
                            <div class="col-8">
                                Mining Data Submissions
                            </div>
                        </div><!-- row -->
                    </div><!-- card -->
                    <div class="card pd-25 mg-t-20">
                        <div class="slim-card-title">Gaming Profiles</div>

                        <div class="media-list mg-t-25">
                            <div class="media">
                                <div><i class="icon ion-link tx-24 lh-0"></i></div>
                                <div class="media-body mg-l-15 mg-t-4">
                                    <h6 class="tx-14 tx-gray-300">Steam</h6>
                                    <a href="" class="d-block">Steam.com/{{$user->username}}/</a>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media mg-t-25">
                                <div><i class="icon ion-social-twitch tx-18 lh-0"></i></div>
                                <div class="media-body mg-l-15 mg-t-2">
                                    <h6 class="tx-14 tx-gray-300">Twitch</h6>
                                    <a href="" class="d-block">Twitch.tv/{{$user->username}}/</a>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media mg-t-25">
                                <div><i class="icon ion-social-tux tx-18 lh-0"></i></div>
                                <div class="media-body mg-l-15 mg-t-2">
                                    <h6 class="tx-14 tx-gray-300">Discord</h6>
                                    <a href="" class="d-block">Discord.gg/{{$user->username}}/</a>
                                </div><!-- media-body -->
                            </div><!-- media -->
                        </div><!-- media-list -->
                    </div><!-- card -->
                    <div class="card card-people-list mg-t-20">
                        <div class="d-flex justify-content-between">
                        <div class="slim-card-title">Ships Owned</div>
                        <div class="float-right text-info"><h4>9</h4></div>
                        </div>
                        <div class="media-list">
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">RSI Freelancer Max</a>
                                    <p>Civilian / Cargo</p> <!--class / type -->
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">RSI URSA Rover</a>
                                    <p>Industrial / Vehicle</p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">Drake Cutlass</a>
                                    <p>Civilian / Dropship</p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">AGIES Sabre Raven</a>
                                    <p>Military / Fighter</p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">MISC Starfarer</a>
                                    <p>Industrial / Cargo</p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">MISC Starfarer</a>
                                    <p>Industrial / Cargo</p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">MISC Starfarer</a>
                                    <p>Industrial / Cargo</p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">MISC Starfarer</a>
                                    <p>Industrial / Cargo</p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <a href="">MISC Starfarer</a>
                                    <p>Industrial / Cargo</p>
                                </div><!-- media-body -->
                            </div><!-- media -->

                        </div><!-- media-list -->
                    </div><!-- card -->
                </div><!-- col-4 -->
            </div><!-- row -->

        </div><!-- container -->
    </div><!-- slim-mainpanel -->
@endsection