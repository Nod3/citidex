<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>The Merchant Marine | Project Citidex</title>

    <!-- vendor css -->
    <link href="/citidex/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/citidex/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="/citidex/lib/chartist/css/chartist.css" rel="stylesheet">
    <link href="/citidex/lib/rickshaw/css/rickshaw.min.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="/css/slim.css">
    <link rel="stylesheet" href="/css/slim.one.css">
    <link rel="stylesheet" href="/css/nuter.css">
    @yield('header')

</head>
<body>
<div class="slim-header">
    <div class="container">
        <div class="slim-header-left">
            <h2 class="slim-logo"><a href="/">Citidex<span>.</span></a></h2>

            <!--  <div class="search-box">
                 <input type="text" class="form-control" placeholder="Search">
                 <button class="btn btn-primary"><i class="fa fa-search"></i></button>
             </div>search-box -->
        </div><!-- slim-header-left -->
        <div class="slim-header-right">
            <!-- <div class="dropdown dropdown-a">
                <a href="" class="header-notification" data-toggle="dropdown">
                    <i class="icon ion-ios-bolt-outline"></i>
                </a>
                <div class="dropdown-menu">
                    <div class="dropdown-menu-header">
                        <h6 class="dropdown-menu-title">Activity Logs</h6>
                        <div>
                            <a href="">Filter List</a>
                            <a href="">Settings</a>
                        </div>
                    </div>
                    <div class="dropdown-activity-list">
                        <div class="activity-label">Today, December 13, 2017</div>
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">10:15am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                                <div class="col-8">Purchased christmas sale cloud storage</div>
                            </div>
                        </div>
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">9:48am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-danger"></span></div>
                                <div class="col-8">Login failure</div>
                            </div>
                        </div>
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">7:29am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-warning"></span></div>
                                <div class="col-8">(D:) Storage almost full</div>
                            </div>
                        </div>
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">3:21am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                                <div class="col-8">1 item sold <strong>Christmas bundle</strong></div>
                            </div>
                        </div>
                        <div class="activity-label">Yesterday, December 12, 2017</div>
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">6:57am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                                <div class="col-8">Earn new badge <strong>Elite Author</strong></div>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-list-footer">
                        <a href="page-activity.html"><i class="fa fa-angle-down"></i> Show All Activities</a>
                    </div>
                </div>
            </div>
            <div class="dropdown dropdown-b">
                <a href="" class="header-notification" data-toggle="dropdown">
                    <i class="icon ion-ios-bell-outline"></i>
                    <span class="indicator"></span>
                </a>
                <div class="dropdown-menu">
                    <div class="dropdown-menu-header">
                        <h6 class="dropdown-menu-title">Notifications</h6>
                        <div>
                            <a href="">Mark All as Read</a>
                            <a href="">Settings</a>
                        </div>
                    </div>
                    <div class="dropdown-list">

                        <a href="" class="dropdown-link">
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <p><strong>Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                                    <span>October 03, 2017 8:45am</span>
                                </div>
                            </div>
                        </a>

                        <a href="" class="dropdown-link">
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <p><strong>Mellisa Brown</strong> appreciated your work <strong>The Social Network</strong></p>
                                    <span>October 02, 2017 12:44am</span>
                                </div>
                            </div>
                        </a>
                        <a href="" class="dropdown-link read">
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <p>20+ new items added are for sale in your <strong>Sale Group</strong></p>
                                    <span>October 01, 2017 10:20pm</span>
                                </div>
                            </div>
                        </a>
                        <a href="" class="dropdown-link read">
                            <div class="media">
                                <img src="http://via.placeholder.com/500x500" alt="">
                                <div class="media-body">
                                    <p><strong>Julius Erving</strong> wants to connect with you on your conversation with <strong>Ronnie Mara</strong></p>
                                    <span>October 01, 2017 6:08pm</span>
                                </div>
                            </div>
                        </a>
                        <div class="dropdown-list-footer">
                            <a href="page-notifications.html"><i class="fa fa-angle-down"></i> Show All Notifications</a>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="dropdown dropdown-c">
                <a href="#" class="logged-user" data-toggle="dropdown">
                    <img src="http://via.placeholder.com/500x500" alt="">
                    <span>{{ucfirst(auth()->user()->username)}}</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <nav class="nav">
                        <a href="/profile/{{Auth::user()->id}}" class="nav-link"><i class="icon ion-person"></i> View Profile</a>

                        <a class="nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="icon ion-forward"></i>
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </nav>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </div><!-- header-right -->
    </div><!-- container -->
</div><!-- slim-header -->

<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/">
                    <i class="icon ion-ios-home-outline"></i>
                    <span>Dashboard</span>
                    <span class="square-8"></span>
                </a>
            </li>
            <li class="nav-item with-sub">
                <a class="nav-link" href="#">
                    <i class="icon ion-ios-book-outline"></i>
                    <span>Pages</span>
                </a>
                <div class="sub-item">
                    <ul>
                        <li><a href="/page/ships">Ships</a></li>
                        <li class="sub-with-sub">
                            <a href="#">Locations & Stores</a>
                            <ul>
                                <li><a href="/locations">Locations</a></li>
                                <li><a href="/stores">Stores</a></li>
                            </ul>
                        </li>
                        <li><a href="/commodities">Commodities</a></li>
                        <li><a href="/items">Personal Items</a></li>
                    </ul>
                </div><!-- dropdown-menu -->
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="icon ion-connection-bars"></i>
                    <span>Mining</span>
                    <span class="square-8"></span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="icon ion-ios-pricetags-outline"></i>
                    <span>Trading</span>
                    <span class="square-8"></span>
                </a>
            </li>
            <li class="nav-item with-sub">
                <a class="nav-link">
                    <i class="icon ion-ios-analytics-outline"></i>
                    <span>Admin</span>
                </a>
                <div class="sub-item">
                    <ul>
                        <li><a href="/admin/ship">Dashboard</a></li>
                        <hr>
                        <p class="text-center text-info">CREATE NEW</p>
                        <li><a href="/admin/ship">Ship</a></li>
                        <li><a href="/admin/location">Location</a></li>
                        <li><a href="/admin/store">Store</a></li>
                        <li><a href="/admin/commodity">Commodity</a></li>
                        <hr>
                        </li>
                    </ul>
                </div><!-- dropdown-menu -->
            </li>
        </ul>
    </div><!-- container -->
</div><!-- slim-navbar -->
@yield('citidex')

<div class="slim-footer">
    <div class="container">
        <p>Copyright 2019 &copy; All Rights Reserved. Citidex</p>
        <p>Designed by: <a href="#">jTech Systems</a></p>
    </div><!-- container -->
</div><!-- slim-footer -->

<script src="/citidex/lib/jquery/js/jquery.js"></script>
<script src="/citidex/lib/popper.js/js/popper.js"></script>
<script src="/citidex/lib/bootstrap/js/bootstrap.js"></script>
<script src="/citidex/lib/jquery.cookie/js/jquery.cookie.js"></script>
<script src="/citidex/lib/chartist/js/chartist.js"></script>
<script src="/citidex/lib/d3/js/d3.js"></script>
<script src="/citidex/lib/rickshaw/js/rickshaw.min.js"></script>
<script src="/citidex/lib/jquery.sparkline.bower/js/jquery.sparkline.min.js"></script>

<script src="/js/ResizeSensor.js"></script>
<script src="/js/dashboard.js"></script>
<script src="/js/slim.js"></script>
@yield('scripts')
</body>
</html>
