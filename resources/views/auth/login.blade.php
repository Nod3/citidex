<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>The Merchant Marine | Citidex</title>

    <!-- Vendor css -->
    <link href="citidex/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="citidex/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="css/slim.css">
    <link rel="stylesheet" href="css/slim.one.css">


</head>
<body>

<div class="d-md-flex flex-row-reverse">
    <div class="signin-right">

        <div class="signin-box">
            <h2 class="signin-title-primary">Welcome Merchants.</h2>
            <h3 class="signin-title-secondary">Sign in to Citidex</h3>

            <form method="POST" action="{{ route('login') }}">
                @csrf
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Username" value="{{ old('username') }}" name="username" id="username" required autofocus>
            </div><!-- form-group -->
            <div class="form-group mg-b-50">
                <input type="password" class="form-control" name="password" id="password" placeholder="Password" required autocomplete="current-password">
            </div><!-- form-group -->
            <button type="submit" class="btn btn-primary btn-block btn-signin">{{ __('Login') }}</button>
            </form>
            <p class="mg-b-0">Don't have an account? <a href="{{route('register')}}">Sign Up</a></p>
        </div>

    </div><!-- signin-right -->
    <div class="signin-left">
        <div class="signin-box">
            <h2 class="slim-logo"><a href="index.html">Citidex<span>.</span></a></h2>
            <p>The Citidex is a project developed from the <a href="#">Merchant Marine</a>, we are excited to release our technology to the community of Star Citizen!!</p>

            <p>Citidex comes from the words, Citizen & Index clearly. Unlike a wiki site, we provide meta data of subject objects in Star Citizen and apply them using our Magic and give you actionable data.</p>

            <p><a href="" class="btn btn-outline-secondary pd-x-25">Learn More</a></p>

            <p class="tx-12">&copy; Copyright 2018. All Rights Reserved.</p>
        </div>
    </div><!-- signin-left -->
</div><!-- d-flex -->

<script src="citidex/lib/jquery/js/jquery.js"></script>
<script src="citidex/lib/popper.js/js/popper.js"></script>
<script src="citidex/lib/bootstrap/js/bootstrap.js"></script>

<script src="js/slim.js"></script>

</body>
</html>
