<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Slim Responsive Bootstrap 4 Admin Template</title>

    <!-- Vendor css -->
    <link href="citidex/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="citidex/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="css/slim.css">
    <link rel="stylesheet" href="css/slim.one.css">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery-3.4.1.js') }}"></script>

</head>
<body>

<div class="d-md-flex flex-row-reverse">
    <div class="signin-right">

        <div class="signin-box signup">
            <h3 class="signin-title-primary">Get Started!</h3>
            <h5 class="signin-title-secondary lh-4">It's free to signup and only takes a minute.</h5>


            <form method="POST" action="{{ route('register') }}">
                @csrf
            <div class="row row-xs mg-b-10">
                <div class="col-sm"><input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="UserName" required autofocus></div>
                <div class="col-sm mg-t-10 mg-sm-t-0"><input id="rsi" type="rsi" class="form-control" name="rsi" value="{{ old('rsi') }}"placeholder="RSI Handle" required></div>
            </div><!-- row -->
            <div class="row row-xs mg-b-10">
                <div class="col-sm"><input id="password" type="password" class="form-control" name="password" placeholder="Password" required></div>
                <div class="col-sm mg-t-10 mg-sm-t-0"><input id="confirm" type="password" class="form-control" placeholder="Confirm Password" name="confirm" required></div>
            </div><!-- row -->
<br><hr><br>
                <button type="submit" class="btn btn-primary btn-block btn-signin">{{ __('Register') }}</button>
            </form>

            <p class="mg-t-40 mg-b-0">Already have an account? <a href="{{route('login')}}">Sign In</a></p>
        </div><!-- signin-box -->

    </div><!-- signin-right -->
    <div class="signin-left">
        <div class="signin-box">
            <h2 class="slim-logo"><a href="index.html">Citidex<span>.</span></a></h2>

            <p>The Citidex is a project developed from the <a href="#">Merchant Marine</a>, we are excited to release our technology to the community of Star Citizen!!</p>

            <p>Citidex comes from the words, Citizen & Index clearly. Unlike a wiki site, we provide meta data of subject objects in Star Citizen and apply them using our Magic and give you actionable data.</p>

            <p><a href="" class="btn btn-outline-secondary pd-x-25">Learn More</a></p>

            <p class="tx-12">&copy; Copyright 2018. All Rights Reserved.</p>
        </div>
    </div><!-- signin-left -->
</div><!-- d-flex -->

<script src="citidex/lib/jquery/js/jquery.js"></script>
<script src="citidex/lib/popper.js/js/popper.js"></script>
<script src="citidex/lib/bootstrap/js/bootstrap.js"></script>

<script src="js/slim.js"></script>

</body>
</html>
